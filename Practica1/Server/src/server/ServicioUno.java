/*
Clase donde se ofrece el servicio 1
*/
package server;

/**
 *
 * @author frnd_
 */
public class ServicioUno {
    
    
    public ServicioUno(){
        System.out.println("Está utilizando el servicio uno");
    }
    
    String may(String cadena){
        return cadena.toUpperCase();
    }
    
    String min(String cadena){
        return cadena.toLowerCase();
    }
    
    String inv(String cadena){
        String invertida="";
        for(int i = cadena.length() - 1 ; i >= 0; i--){
            invertida = invertida + cadena.charAt(i);
        }
        System.out.println(invertida);
        return invertida;
    }
    
    int len(String cadena){
        return cadena.length();
    }
    
    int alea(int lim){
        return (int) (Math.random()*lim);
    }
    
}
