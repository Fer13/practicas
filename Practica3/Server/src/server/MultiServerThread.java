package server;

import java.net.*;
import java.io.*;
import java.util.Date;

public class MultiServerThread extends Thread {
   
   private Socket socket = null;
   private int bandera = 1;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         ServicioUno servUno = new ServicioUno();
         
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
	
         
	 while((lineIn = entrada.readLine()) != null ){
            System.out.println("Received: "+lineIn);
            System.out.println("Clientes conectados: " + ServerMultiClient.NoClients);
            escritor.flush();
            
            if(lineIn.equals("FIN")){
                ServerMultiClient.NoClients--;
                break;
            }else{
                /*A continuacion se valida el mensaje del usuario almacenado
                  en lineIn
                */
                String comando,parametro;
                int noParam;
                String Mensaje = lineIn.substring(1);
                String[] mensaje = Mensaje.split("#");
                comando = mensaje[0];
                noParam = Integer.parseInt(mensaje[1]);
                parametro = mensaje[2];
                char param = parametro.charAt(0);
                
                
                
                switch(comando){
                    case "may":
                        String may = servUno.may(parametro);
                        may = "#R-"+comando+"#"+noParam+"#"+may+"#";
                        escritor.println(may);
                        escritor.flush();
                        break;
                    case "min":
                        String min = servUno.min(parametro);
                        min = "#R-"+comando+"#"+noParam+"#"+min+"#";
                        escritor.println(min);
                        escritor.flush();
                        break;
                    case "inv":
                        String inv = servUno.inv(parametro);
                        inv = "#R-"+comando+"#"+noParam+"#"+inv+"#";
                        escritor.println(inv);
                        escritor.flush();
                        break;
                    case "len":
                        int len = servUno.len(parametro);
                        String Len = "#R-"+comando+"#"+noParam+"#"+len+"#";
                        escritor.println(Len);
                        escritor.flush();
                        break;
                    case "alea":
                        if (Character.isDigit(param)) {
                            int lim = Integer.parseInt(parametro);
                            float alea = servUno.alea(lim);
                            String Alea = "#R-"+comando+"#"+noParam+"#"+alea+"#";
                            escritor.println(Alea);
                            escritor.flush();
                        }else{
                            escritor.println("Atención, parámetro no válido");
                            escritor.flush();
                        }
                        break;
                    case "auth":
                        String usuario = mensaje[2];
                        String pass = mensaje[3];
                        boolean aut = servUno.validaUsuario(usuario,pass);
                        
                        if(aut){
                            String Auth = "#R-"+comando+"#"+"true"+"#";
                            escritor.println(Auth);
                            escritor.flush();
                        }else{
                            String Auth = "#R-"+comando+"#"+"false"+"#";
                            escritor.println(Auth);
                            escritor.flush();
                        
                            try {
                                entrada.close();
                                escritor.close();
                                socket.close();
                                System.exit(0);
                            } catch (Exception e) {
                                System.out.println("Error : " + e.toString());
                                socket.close();
                                System.exit(0);
                            }
                        }
                        break;
                    default:
                        escritor.println("Comando: "+comando+" no valido");
                        escritor.flush();
                }
            }
         }
         
         
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	 }
         
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 
